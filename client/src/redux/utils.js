import { createAction } from "@reduxjs/toolkit";

export const createRequestActions = requestActionName => ({
  request: createAction(`${requestActionName}_REQUEST`),
  success: createAction(`${requestActionName}_SUCCESS`),
  error: createAction(`${requestActionName}_ERROR`)
});
