import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { dataReducer } from "./slices/data";
import { sagaMiddleware, runSagaMiddleware } from "./sagas";

export default function configureAppStore() {
  const store = configureStore({
    reducer: {
      data: dataReducer
    },
    middleware: [sagaMiddleware, ...getDefaultMiddleware()]
  });

  runSagaMiddleware();

  return store;
}
