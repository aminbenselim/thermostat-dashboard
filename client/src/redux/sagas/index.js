import createSagaMiddleware from "redux-saga";

import dataFetcherSaga from "./dataFetcher";

export const sagaMiddleware = createSagaMiddleware();
export const runSagaMiddleware = () => sagaMiddleware.run(dataFetcherSaga);
