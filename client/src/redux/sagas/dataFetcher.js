import {
  put,
  call,
  all,
  delay,
  race,
  take,
  select,
  fork,
  takeLatest
} from "redux-saga/effects";
import {
  fetchThermostatData,
  pushReadingToListOfReadings,
  updateSetpoint
} from "../slices/data";

const serverUrl = "http://localhost:9090";

function* fetchDataPolling(action) {
  while (true) {
    try {
      const response = yield call(fetch, serverUrl);

      if (response.status !== 200) {
        // we retry the request again if we receive a 202
        yield* fetchDataPolling(action);
      }

      const data = yield response.json();
      yield put(fetchThermostatData.success(data));
      yield put(pushReadingToListOfReadings(data));

      // we fetch the data every 2 seconds
      yield delay(2000);
    } catch (error) {
      yield put(fetchThermostatData.error());
    }
  }
}

function* patchRequest(action) {
  try {
    const result = yield call(fetch, serverUrl, {
      method: "PATCH",
      body: JSON.stringify({
        currentSetpoint: action.payload
      }),
      // because the backend usees bodyParser.json(), we need to specify the header
      headers: {
        "Content-Type": "application/json"
      }
    });

    yield put(updateSetpoint.success());
  } catch (error) {
    yield put(updateSetpoint.error());
  }
}

export default function* rootSaga() {
  yield all([
    // We use race here because in case there is an error in any of the requests,
    // we want to stop polling. Also because the update request takes 1 second,
    // we stop polling after the update is successful, and then start it again to
    // be able to receive the new value
    race([
      call(fetchDataPolling),
      take(fetchThermostatData.error.type),
      take(updateSetpoint.error.type),
      take(updateSetpoint.success.type)
    ]),
    takeLatest(updateSetpoint.request.type, patchRequest),
    // here is where we start polling again after successfully updating
    takeLatest(updateSetpoint.success.type, fetchDataPolling)
  ]);
}
