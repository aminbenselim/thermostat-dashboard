import { createSlice } from "@reduxjs/toolkit";
import { createRequestActions } from "../utils";

const fetchThermostatData = createRequestActions("FETCH_DATA");
const updateSetpoint = createRequestActions("UPDATE_SETPOINT");

const dataSlice = createSlice({
  name: "data",
  initialState: {
    isUpdating: false,
    isLoading: false,
    requestFailed: false,
    currentReading: {},
    allReadings: []
  },
  reducers: {
    pushReadingToListOfReadings: (state, action) => {
      state.allReadings.push(action.payload);
    }
  },
  extraReducers: {
    [fetchThermostatData.request.type]: state => {
      state.isLoading = true;
    },
    [fetchThermostatData.success.type]: (state, action) => {
      state.isLoading = false;
      state.currentReading = action.payload;
    },
    [fetchThermostatData.error.type]: (state, action) => {
      state.isLoading = false;
      state.requestFailed = true;
    },
    [updateSetpoint.request.type]: state => {
      state.isUpdating = true;
    },
    [updateSetpoint.success.type]: (state, action) => {
      state.isUpdating = false;
    },
    [updateSetpoint.error.type]: (state, action) => {
      state.isUpdating = false;
    }
  }
});
const {
  actions: { pushReadingToListOfReadings },
  reducer: dataReducer
} = dataSlice;

export {
  fetchThermostatData,
  pushReadingToListOfReadings,
  dataReducer,
  updateSetpoint
};
