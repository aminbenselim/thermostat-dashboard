import en from "./en";
import nl from "./nl";

const useI18n = () => {
  const messages = {
    en,
    nl
  };

  const locale = navigator.language.split("-")[0];

  return {
    locale,
    messages: messages[locale]
  };
};

export default useI18n;
