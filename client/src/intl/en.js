export default {
  "app.title": "Thermostat Dashboard!",
  "app.dashboard.currentSetpoint.label": "Current Set Temperature",
  "app.dashboard.currentSetpoint.increment": "Increment",
  "app.dashboard.currentSetpoint.decrement": "Decrement",
  "app.dashboard.currentTemp.label": "Current Temperature",
  "app.dashboard.lastUpdateAt": "value last retrieved on {time}"
};
