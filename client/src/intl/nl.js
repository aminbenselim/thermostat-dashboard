export default {
  "app.title": "Thermostaatdashboard!",
  "app.dashboard.currentSetpoint.label": "Huidige ingestelde temperatuur",
  "app.dashboard.currentSetpoint.increment": "aanwas",
  "app.dashboard.currentSetpoint.decrement": "decrement",
  "app.dashboard.currentTemp.label": "Huidige temperatuur",
  "app.dashboard.lastUpdateAt": "waarde laatst opgehaald op {time}"
};
