import React from "react";
import { useSelector, useDispatch } from "react-redux";
import DataDisplay from "./DataDisplay";
import { updateSetpoint } from "../redux/slices/data";

const DataDisplayConnector = props => {
  const dispatch = useDispatch();
  const currentReading = useSelector(state => state.data.currentReading);
  const isUpdating = useSelector(state => state.data.isUpdating);

  const lastUpdatedAt = new Date(
    currentReading?.timestamp
  ).toLocaleTimeString();

  const incrementReading = () =>
    dispatch(updateSetpoint.request(currentReading?.currentSetpoint + 0.5));

  const decrementReading = () =>
    dispatch(updateSetpoint.request(currentReading?.currentSetpoint - 0.5));

  return (
    <DataDisplay
      currentTemp={currentReading?.currentTemp}
      currentSetpoint={currentReading?.currentSetpoint}
      lastUpdatedAt={lastUpdatedAt}
      incrementReading={incrementReading}
      decrementReading={decrementReading}
      isUpdating={isUpdating}
    />
  );
};

export default DataDisplayConnector;
