import React from "react";
import { Layout } from "antd";
import { IntlProvider, FormattedMessage } from "react-intl";
import DataDisplay from "./DataDisplayConnector";
import useI18n from "../intl/useI18n";

const { Header, Content, Footer } = Layout;

const App = () => {
  const { locale, messages } = useI18n();

  return (
    <IntlProvider locale={locale} messages={messages}>
      <Layout>
        <Header>
          <h1 style={{ color: "white" }}>
            <FormattedMessage id="app.title" />
          </h1>
        </Header>
        <Content style={{ padding: "0 50px" }}>
          <DataDisplay />
        </Content>
      </Layout>
    </IntlProvider>
  );
};

export default App;
