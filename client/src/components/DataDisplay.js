import React from "react";
import { Statistic, Row, Col, Button, Icon } from "antd";
import { useIntl, FormattedMessage } from "react-intl";

const DataDisplay = props => {
  const intl = useIntl();

  return (
    <>
      <Row gutter={16}>
        <Col span={12}>
          <Statistic
            title={intl.formatMessage({
              id: "app.dashboard.currentSetpoint.label"
            })}
            value={props.currentSetpoint}
            precision={2}
            formatter={val => <span data-testid="current-setpoint">{val}</span>}
          />
          <Button.Group size="large">
            <Button
              type="primary"
              onClick={props.incrementReading}
              disabled={props.isUpdating}
            >
              <Icon type={props.isUpdating ? "loading" : "up"} />
              <FormattedMessage id="app.dashboard.currentSetpoint.increment" />
            </Button>
            <Button
              type="primary"
              onClick={props.decrementReading}
              disabled={props.isUpdating}
            >
              <FormattedMessage id="app.dashboard.currentSetpoint.decrement" />
              <Icon type={props.isUpdating ? "loading" : "down"} />
            </Button>
          </Button.Group>
        </Col>
        <Col span={12}>
          <Statistic
            title={intl.formatMessage({
              id: "app.dashboard.currentTemp.label"
            })}
            value={props.currentTemp}
            precision={2}
            formatter={val => <span data-testid="current-temp">{val}</span>}
          />
        </Col>
      </Row>
      <Row>
        <FormattedMessage
          id="app.dashboard.lastUpdateAt"
          values={{
            time: props.lastUpdatedAt
          }}
        />
      </Row>
    </>
  );
};

export default DataDisplay;
