import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

const rootElement = document.getElementById("root");

function renderApp() {
  const App = require("./components/App").default;
  const configureAppStore = require("./redux/store").default;

  const store = configureAppStore();
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    rootElement
  );
}

renderApp();

if (module.hot) {
  module.hot.accept(renderApp);
}
