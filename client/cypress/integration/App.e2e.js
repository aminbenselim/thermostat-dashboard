describe("Happy path", () => {
  it("should have a header", () => {
    cy.visit("/");
    cy.findByText("Thermostat Dashboard!").should("be.visible");
  });

  it("should display current data", () => {
    cy.findByText("Current Set Temperature").should("be.visible");

    cy.findByTestId("current-setpoint").should("be.visible");
    cy.findByTestId("current-temp").should("be.visible");
  });

  it("should display Increment and Decrement buttons", () => {
    cy.findByText("Increment").should("be.visible");
    cy.findByText("Decrement").should("be.visible");
  });

  it("should increase current setpoint by 0.5", () => {
    cy.queryByTestId("current-setpoint")
      .invoke("text")
      .then(val => {
        cy.findByText("Increment").click();

        const newValue = +val + 0.5;
        cy.findByText(newValue.toString());
      });
  });

  it("should decrease current setpoint by 0.5", () => {
    cy.queryByTestId("current-setpoint")
      .invoke("text")
      .then(val => {
        cy.findByText("Decrement").click();

        const newValue = +val - 0.5;
        cy.findByText(newValue.toString());
      });
  });
});

describe("App I18n", () => {
  it("should have strings translated to Dutch", () => {
    cy.visit("/", {
      onBeforeLoad: _contentWindow => {
        Object.defineProperty(_contentWindow.navigator, "language", {
          value: "nl"
        });
      }
    });
    cy.findByText("Thermostaatdashboard!").should("be.visible");
    cy.findByText("Huidige ingestelde temperatuur").should("be.visible");
    cy.findByText("aanwas").should("be.visible");
  });
});
