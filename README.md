# Thermostat Dashboard

Dahsboard that shows the following information:

- Current temperature in the room.
- Current set temperature.
- Time of latest update.
- Button to increment the temperature by half a degree at the time.
- Button to decrement the temperatture by half a degree at the time.

We fetch the data at least every 2 seconds to update the current temperature and the time of last update.
Be aware of race conditions. The patch api call takes about 1 second to update the data.
It also is possible that when you get the data, you will receive a `202` HTTP-code instead of the data.
This means that the backend has received your request ,but it can not send you the latest data. When this happens, you need to retry the API call. (The backend is set to have a 50-50 chance to send the data or return a `202`).

## API

the API will start at <http://localhost:9090>

This will spin up a node express server for the API.
There are 2 endpoints for this api.

| url                      | method |
| ------------------------ | ------ |
| <http://localhost:9090/> | GET    |
| <http://localhost:9090/> | PATCH  |
|                          |        |

## Client

The client website will start at <http://localhost:1234>
